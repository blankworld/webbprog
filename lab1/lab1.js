'use strict';
const imported = require('./inventory.js');
let inventory = imported.inventory;

function filter(itemType) {
	return Object.keys(inventory).filter(item => inventory[item][itemType]);
}

console.log("Foundations: " + filter("foundation").toString() + "\n");
console.log("Proteins: " + filter("protein").toString() + "\n");
console.log("Extras: " + filter("extra").toString() + "\n");
console.log("Dressings: " + filter("dressing").toString() + "\n");

class Salad {

	constructor() {
		this.ingredients = [];
	}
	
	addIngredient(args) {
		this.ingredients.push({...args, ...inventory[args.name]});
	}
	
	removeIngredient(selection) {
		this.ingredients = this.ingredients.filter(item => item.name !== selection);
	}		
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price, 0);
	}
	
	toString() {
		return this.ingredients.map(item => item.name).toString();
	}
	
	getType() {
		return "Regular";
	}
}

class ExtraGreenSalad extends Salad {
	constructor() {
		super();
	}
	
	addIngredient(args) {
		super.addIngredient({...args, size:(inventory[args.name].foundation) ? 1.3 : 0.5});
	}
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price*item.size, 0);
	}
	
	getType() {
		return "Extra green";
	}
}

class GourmetSalad extends Salad {
	constructor() {
		super();
	}
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price*item.size, 0);
	}
	
	toString() {
		return this.ingredients.map(item => item.name + "(" + item.size + ")").toString();
	}
	
	getType() {
		return "Gourmet";
	}
}

// receptet innhåller ingredienser som inte finns

let myCesarSalad = new Salad();
//let myCesarSalad = new ExtraGreenSalad();
//let myCesarSalad = new GourmetSalad();
myCesarSalad.addIngredient({name:"Sallad"});
myCesarSalad.addIngredient({name:"Kycklingfilé"});
myCesarSalad.addIngredient({name:"Krutonger"});
myCesarSalad.addIngredient({name:"Tomat"});
myCesarSalad.addIngredient({name:"Chèvreost"});
myCesarSalad.addIngredient({name:"Ceasardressing"});

/*myCesarSalad.addIngredient({name:"Sallad", size:1.3});
myCesarSalad.addIngredient({name:"Kycklingfilé", size:0.5});
myCesarSalad.addIngredient({name:"Krutonger", size:0.5});
myCesarSalad.addIngredient({name:"Tomat", size:0.5});
myCesarSalad.addIngredient({name:"Chèvreost", size:0.5});
myCesarSalad.addIngredient({name:"Ceasardressing", size:0.5});*/
console.log("Price: " + myCesarSalad.price());



var manager = new OrderManager(new ShoppingCart());
repl.start({ prompt: manager.generateInstruction(),
  eval: function(input, context, filename, cb) {
    manager = manager.interpretResponse(input);
    cb(null);
  }
});