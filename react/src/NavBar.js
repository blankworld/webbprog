import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavBar extends Component {	
	constructor(props) {
		super(props);
		this.state = {pages: this.props.pages};
	}
	
	componentDidMount() {
		this.props.history.push(this.props.location.pathname);
	}
	
	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			this.onRouteChanged(this.props.location.pathname);
		}
	}

	onRouteChanged(path) {
		this.setState({pages:this.state.pages.map(page => ((page.path === path) ? {...page, active:true} : {...page, active:false}))});
	}

	render() {
		return (
		<nav className="navbar navbar-expand-sm bg-primary navbar-dark" style={{marginBottom:'16px'}}>
			<Link className="navbar-brand" to={'/'}></Link>
			<button className="navbar-toggler" id="menu-button" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="collapsibleNavbar">
				<ul className="navbar-nav">
					{this.state.pages.map(page => 
					<li className={(page.active) ? "nav-item active" : "nav-item"} key={page.name + 1}>
						<Link className="nav-link" key={page.name} to={page.path}>{page.name}</Link>
					</li>
					)}   
				</ul>
			</div>  
		</nav>
		);
	}
}

export default NavBar;