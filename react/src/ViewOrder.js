import React, { Component } from 'react';

class ViewOrder extends Component {	
	render() {		
		return (
			<div className="row-container">
				<h3>Din beställning</h3>
				<ul className="list-group">
					<li className="list-group-item d-flex justify-content-between align-items-center">
						<span className="badge badge-primary badge-pill"></span>
					</li>
					{this.props.orders.map(order =>
						<li key={order.string + 2} className="list-group-item d-flex justify-content-between align-items-center">
							{order.string}
							<span key={order.string + 1} className="badge badge-primary badge-pill">{order.price + " kr"}</span>
						</li>)
					}
				</ul>
			</div>
		);
	}
}

export default ViewOrder;