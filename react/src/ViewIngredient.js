import React, { Component } from 'react';

class ViewIngredient extends Component {	
	render() {		
		return (
			<div className="row-container">
				<h3>{this.props.match.params.name}</h3>
				<ul className="list-group">
					<li className="list-group-item d-flex justify-content-between align-items-center">
						<span className="badge badge-primary badge-pill"></span>
					</li>
					{Object.keys(this.props.inventory[this.props.match.params.name]).map(item =>
						<li key={item} className="list-group-item d-flex justify-content-between align-items-center">
							{(item === 'price') ? "price: " + this.props.inventory[this.props.match.params.name].price + " kr" : item}
						</li>)
					}
				</ul>
			</div>
		);
	}
}

export default ViewIngredient;