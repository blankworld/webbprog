import React, { Component } from 'react';

class NotFound extends Component {	
	render() {		
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-12">
						<div className="error-template">
							<h1>Oops!</h1>
							<h2>404 Not Found</h2>
							<div className="error-details">Sidan kunde inte hittas!</div>
							<div className="error-actions">
								<a href="./" className="btn btn-primary btn-lg"><span className="glyphicon glyphicon-home"></span>
									Ta mig hem </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default NotFound;