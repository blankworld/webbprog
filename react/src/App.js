import inventory from './inventory.ES6';
import ComposeSalad from './ComposeSalad';
import React, { Component } from 'react';
//import ReactModal from './ReactModal';
import ViewOrder from './ViewOrder';
import NavBar from './NavBar';
import NotFound from './NotFound';
import ViewIngredient from './ViewIngredient';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

class App extends Component {
  constructor(props) {
	super(props);
	this.state = {orders: [] };
  }	
  
  addOrder(order) {
	  this.state.orders.push(order);
	  this.setState({...this.state});
  }
  
  orderInfo() {
		let counter = 1;
		return this.state.orders.map(salad => ({string:`${counter++ + ", " + salad.toString()}`, price:`${salad.price()}`}));
   }
	
  render() {
	
	const composeSaladElem = (params) => <ComposeSalad {...params} inventory={inventory} submit={e => this.addOrder(e)} /> 
	const viewOrderElem = (params) => <ViewOrder {...params} orders={this.orderInfo()}/>
	const pages = [{name:'Hem', path:'/', active:true}, {name:'Komponera din egen sallad', path:'/compose-salad', active:false}, {name:'Din beställning', path:'/view-order', active:false}];
	const navbar = (params) => <NavBar {...params} pages={pages}/>
	const viewIngredientElem = (params) => <ViewIngredient {...params} inventory={inventory}/>
	
    return (
	<Router>
      <div className="App">
		<div className="jumbotron text-center" style={{marginBottom:'0px'}}><h1>Salad bar</h1></div>
		<Route path="/" render={navbar} />
		<div className="container">
		<Switch>
			<Route exact path="/" />
			<Route path="/compose-salad" render={composeSaladElem} />
			<Route path="/view-order" render={viewOrderElem} />
			<Route path='/view-ingredient/:name' render={viewIngredientElem} /> 
			<Route component={NotFound} />
		</Switch>
		</div>
		<footer className="footer font-small bg-primary text-white" style={{marginTop:'16px'}}>
			<div className="footer-copyright text-center py-3">© 2019 Copyright</div>
		</footer>
      </div>
	  </Router>
    );
  }
}

/*
<ReactModal id={this.modalID} title="Komponera din egen salad">
<button type="button" data-toggle="modal" data-target={'#' + this.modalID} className="btn btn-primary">Komponera en salad</button>
<ViewOrder orders={this.orderInfo()}/>
*/

export default App;