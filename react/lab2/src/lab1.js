'use strict';
const imported = require('./inventory.js');
let inventory = imported.inventory;

class Salad {

	constructor() {
		this.ingredients = [];
	}
	
	addIngredient(args) {
		this.ingredients.push({...args, ...inventory[args.name]});
	}
	
	removeIngredient(selection) {
		this.ingredients = this.ingredients.filter(item => item.name !== selection);
	}		
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price, 0);
	}
	
	toString() {
		return this.ingredients.map(item => item.name).toString();
	}
	
	getType() {
		return "Regular";
	}
}

class ExtraGreenSalad extends Salad {
	constructor() {
		super();
	}
	
	addIngredient(args) {
		super.addIngredient({...args, size:(inventory[args.name].foundation) ? 1.3 : 0.5});
	}
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price*item.size, 0);
	}
	
	getType() {
		return "Extra green";
	}
}

class GourmetSalad extends Salad {
	constructor() {
		super();
	}
	
	price() {
		return this.ingredients.reduce((total, item) => total + item.price*item.size, 0);
	}
	
	toString() {
		return this.ingredients.map(item => item.name + "(" + item.size + ")").toString();
	}
	
	getType() {
		return "Gourmet";
	}
}

export default Salad;