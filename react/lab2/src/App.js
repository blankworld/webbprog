//import inventory from './inventory.ES6';
import ComposeSalad from './ComposeSalad';
import React, { Component } from 'react';
//import ReactModal from './ReactModal';
import ViewOrder from './ViewOrder';
import NavBar from './NavBar';
import NotFound from './NotFound';
import ViewIngredient from './ViewIngredient';
import Salad from './lab1.js';
import { Route, HashBrowser, BrowserRouter as Router, Switch } from 'react-router-dom';

class App extends Component {
  constructor(props) {
  super(props);
	this.state = {
    orders: [],
    inventory: {}
  };
  }

  componentDidMount() {
	  this.fetchRemoteInventory().then(remoteInventory => this.setState({inventory:remoteInventory}));
	  this.loadOrders();
  }

  fetchItemsByType(type, server_path) {
    return fetch(server_path + type + '/').then(response => response.json())
              .then(foundations => Promise.all(foundations.map(foundation => fetch(server_path + type + '/' + foundation)
              .then(response => response.json())
              .then(item => ({[foundation]:item})))));
  }

  fetchRemoteInventory() {
    const server_path = 'http://localhost:8080/';
    return Promise.all([
                this.fetchItemsByType("foundations", server_path),
                this.fetchItemsByType("proteins", server_path),
                this.fetchItemsByType("extras", server_path),
                this.fetchItemsByType("dressings", server_path)])
                .then(groups => groups.reduce((items, group) => ([...items, ...group]), []))
                .then(items => items.reduce((inventory, item) => ({...inventory, ...item}), {}));
  }

  storeOrders(orders) {
	  if (typeof(Storage) !== "undefined") {
      const ordersString = JSON.stringify(orders);
		 window.localStorage.setItem("orders", ordersString);
     this.postOrders(ordersString);
	  }
  }

  loadOrders() {
	  if (typeof(Storage) !== "undefined") {
		  const orders = window.localStorage.getItem("orders");
		  if (orders !== null) {
      let chainParent = new Salad();
			let storedOrders = JSON.parse(orders).map(order => Object.setPrototypeOf(order, chainParent));
			this.setState({orders: storedOrders});
		  }
	  }
  }

  postOrders(data) {
    fetch('http://localhost:8080/orders/', {
    method: 'POST',
    body: data
  }).then(response => response.json()).then(json => console.log(json));
  }

  addOrder(order) {
	  let tempOrders = [...this.state.orders];
	  tempOrders.push(order);
	  this.storeOrders(tempOrders);
	  this.setState({orders: tempOrders});
  }

  removeOrder(order) {
    let tempOrders = this.state.orders.filter(salad => salad.toString() !== order);
    this.storeOrders(tempOrders);
	  this.setState({
		  orders: tempOrders
	   });
  }

  orderInfo() {
		let counter = 1;
		return this.state.orders.map(salad => ({string:`${counter++ + ", " + salad.toString()}`, price:`${salad.price()}`}));
   }

  render() {

	const composeSaladElem = (params) => <ComposeSalad {...params} inventory={this.state.inventory} submit={e => this.addOrder(e)} />
	const viewOrderElem = (params) => <ViewOrder {...params} removeOrder={order => this.removeOrder(order)} orders={this.orderInfo()}/>
	const pages = [{name:'Hem', path:'/'}, {name:'Komponera din egen sallad', path:'/compose-salad'}, {name:'Din beställning', path:'/view-order'}];
	const navbar = (params) => <NavBar pages={pages}/>
	const viewIngredientElem = (params) => <ViewIngredient {...params} inventory={this.state.inventory}/>

    return (
	<Router>
      <div className="App">
		<div className="jumbotron text-center" style={{marginBottom:'0px'}}><h1>Salad bar</h1></div>
		<Route path="/" render={navbar} />
		<div className="container">
		<Switch>
			<Route exact path="/" />
			<Route path="/compose-salad" render={composeSaladElem} />
			<Route path="/view-order" render={viewOrderElem} />
			<Route path='/view-ingredient/:name' render={viewIngredientElem} />
			<Route component={NotFound} />
		</Switch>
		</div>
		<footer className="footer font-small bg-primary text-white" style={{marginTop:'16px'}}>
			<div className="footer-copyright text-center py-3">© 2019 Copyright</div>
		</footer>
      </div>
	  </Router>
    );
  }
}

/*
<ReactModal id={this.modalID} title="Komponera din egen salad">
<button type="button" data-toggle="modal" data-target={'#' + this.modalID} className="btn btn-primary">Komponera en salad</button>
<ViewOrder orders={this.orderInfo()}/>
*/

export default App;
