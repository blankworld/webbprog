import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ReactModal extends Component {	
	render() {
		return (
		<div className="modal fade" id={this.props.id}>
			<div className="modal-dialog">
				<div className="modal-content">
       
					<div className="modal-header text-primary">
						<h4 className="modal-title">{this.props.title}</h4>
						<button type="button" className="close" data-dismiss="modal">&times;</button>
					</div>
	
					<div className="modal-body">
					{this.props.children}
					</div>
       
				</div>
			</div>
		</div>
		);
	}
}

ReactModal.propTypes = {
  children: PropTypes.node
};

export default ReactModal;