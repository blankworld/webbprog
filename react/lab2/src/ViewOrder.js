import React, { Component } from 'react';

class ViewOrder extends Component {
	constructor(props) {
		super(props);
		this.removeOrder = this.removeOrder.bind(this);
		this.state = {total: 0};
	}

	componentDidUpdate(prevProps) {
		if (prevProps.orders !== this.props.orders) {
			const newTotal = this.props.orders.reduce((total, order) => {total = total + +order.price; return total;}, 0);
			console.log("total:  " + newTotal);
			this.setState({total: newTotal});
		}
	}

	removeOrder(order) {
		this.props.removeOrder(order);
	}

	render() {
		return (
			<div className="row-container">
				<h3>Din beställning</h3>
				<ul className="list-group">
					<li className="list-group-item d-flex justify-content-between align-items-center">
						<span className="badge badge-primary badge-pill"></span>
					</li>
					{this.props.orders.map(order =>
						<li key={order.string + 2} className="list-group-item d-flex justify-content-between align-items-center">
							<button key={order.string + 3} className="btn btn-danger badge-pill" onClick={() => this.removeOrder(order.string.split(', ').splice(1).toString())} >x</button>
							{order.string}
							<span key={order.string + 1} className="badge badge-primary badge-pill">{order.price + " kr"}</span>
						</li>)
					}
					<li className="list-group-item d-flex justify-content-between align-items-center">
						Total: <span className="badge badge-primary badge-pill">{this.state.total + " kr"}</span>
					</li>
				</ul>
			</div>
		);
	}
}

//unary plus: 3 + 4 + '5' = 75 while 3 + 4 + +'5' = 12

export default ViewOrder;
