import React, { Component } from 'react';

class MultipleSelection extends Component {
	constructor(props) {
	super(props);
	this.state = {selections: []};
	}
	
	handleChange(event) {
		const selection = event.target.value;
		const selections = this.state.selections.slice();
		if (selection && !selections.filter(item => item == selection).length) {
			selections.push(selection);
			this.setState({
				selections: selections
			});
		}
		event.target.selectedIndex = 0;
	}
	
	removeSelection(event) {
		const selections = this.state.selections.slice().filter(selection => selection != event.target.value);
		
		this.setState({
			selections: selections
		});
		event.preventDefault();
	}
	
	render() {
		return (
		<>
			<select name={this.props.type} className="custom-select" onChange={e => this.handleChange(e)}>
				<option defaultValue=""></option>
				{this.props.itemFilter(this.props.type).map(name => <option key={name}>{name}</option>)}
			</select>
			{this.state.selections.map(name => 
				<div className="input-group" key={name + 3}>
					<input type="text" disabled key={name + 2} className="form-control" placeholder={name}/>
					<div className="input-group-append" key={name + 1}>
						<button className="btn btn-danger" value={name} key={name} onClick={e => this.removeSelection(e)}>X</button> 
					</div>
				</div>
			)}
		</>
		);
	}
}

export default MultipleSelection;