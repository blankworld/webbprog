import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

class NavBar extends Component {
	render() {
		return (
		<nav className="navbar navbar-expand-sm bg-primary navbar-dark" style={{marginBottom:'16px'}}>
			<Link className="navbar-brand" to={'/'}></Link>
			<button className="navbar-toggler" id="menu-button" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="collapsibleNavbar">
				<ul className="navbar-nav">
					{this.props.pages.map(page =>
					<li className="nav-item" key={page.name + 1}>
						<NavLink className="nav-link" key={page.name} exact to={page.path}>{page.name}</NavLink>
					</li>
					)}
				</ul>
			</div>
		</nav>
		);
	}
}

export default NavBar;
