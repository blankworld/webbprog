import React, { Component } from 'react';
import SaladSelect from './SaladSelect';
import SaladCheckbox from './SaladCheckbox';
import Salad from './lab1.js';

class ComposeSalad extends Component {
	constructor(props) {
	super(props);
	this.inventory = props.inventory;
	const inventoryKeys = Object.keys(this.inventory);
	this.state = {
		formData: {
			foundation:'',
			dressing:'',
			extra: inventoryKeys.filter(key => this.inventory[key].extra).map(item => ({name:item, value:false})),
			protein: inventoryKeys.filter(key => this.inventory[key].protein).map(item => ({name:item, value:false}))
		},
		formErrors: {ignoreInvalid: true, extra: {validity:false, min:4, max:15}, protein: {validity:false, min:1, max:2}}
	};

    this.handleSubmit = this.handleSubmit.bind(this);
	this.handleChange = this.handleChange.bind(this);
	this.handleCheckbox = this.handleCheckbox.bind(this);
	}

	handleChange(event) {
		if (event.target.selectedIndex > -1) {
			let mutatedData = {...this.state.formData};
			mutatedData[event.target.name] = event.target.value;
			this.setState({...this.state,
				formData: mutatedData
			});
			//event.target.parentElement.classList.add("was-validated");
		}
	}

	setFormData() {
		this.inventory = this.props.inventory;
		const inventoryKeys = Object.keys(this.inventory);
		this.setState({
			formData: {
				foundation:'',
				dressing:'',
				extra: inventoryKeys.filter(key => this.inventory[key].extra).map(item => ({name:item, value:false})),
				protein: inventoryKeys.filter(key => this.inventory[key].protein).map(item => ({name:item, value:false}))
				}
		});
	}

	componentDidUpdate(prevProps) {
		if (this.props.inventory !== this.inventory) {
			this.setFormData();
		}
	}

	handleCheckbox(event) {
		let mutatedData = {...this.state.formData};
		let mutatedErrors = {...this.state.formErrors};
		const name = event.target.name;
		const type = this.getType(name);
		if (event.target.checked) {
			mutatedData[type] = mutatedData[type].map(item => (item.name === name) ? {...item, value:true} : item);
		} else {
			mutatedData[type] = mutatedData[type].map(item => (item.name === name) ? {...item, value:false} : item);
		}
		mutatedErrors[type].validity = this.checkboxValidity(mutatedErrors[type], mutatedData[type].filter(item => item.value));
		this.setState({
			formData: mutatedData,
			formErrors: mutatedErrors
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		event.target.parentElement.classList.add("was-validated");
		const validity = this.validateForm();

		if(event.target.checkValidity() && validity){
			this.props.submit(this.composeSalad());
			event.target.reset();
			event.target.parentElement.classList.remove("was-validated");
			const tempErrors = this.state.formErrors;
			const tempForm = this.state.formData;
			this.setState({
				formErrors: {ignoreInvalid: true, extra: {...tempErrors.extra, validity:false}, protein: {...tempErrors.protein, validity:false}},
				formData: {
					foundation:'',
					dressing:'',
					extra: {...tempForm.extra, value:false},
					protein: {...tempForm.protein, value:false}
					}
			});
			this.props.history.push('/view-order');
		} else if (!validity) {
			let mutatedErrors = {...this.state.formErrors};
			mutatedErrors.ignoreInvalid = false;
			this.setState({...this.state,
				formErrors: mutatedErrors
			});
		}
	}

	checkboxValidity(group, source) {
		return (source.length >= group.min && source.length <= group.max);
	}

	getSelectData(type) {
		return Object.keys(this.inventory).filter(key => this.inventory[key][type]).map(item => ({name:type, value:item, text: item + "+" + this.inventory[item].price + " kr"}))
	}

	getCheckboxData(type) {
		return this.state.formData[type].map(item => ({...item, text: item.name + "+" + this.inventory[item.name].price + " kr"}))
	}

	checkGroupValidity(type) {
		const errors = this.state.formErrors;
		return errors[type].validity || errors.ignoreInvalid;
	}

	composeSalad() {
		let salad = new Salad();
		const tempForm = this.state.formData;
		const formKeys = Object.keys(tempForm);
		salad = formKeys.filter(type => typeof tempForm[type] === "string").reduce((list, type) => {list.push(tempForm[type]); return list;}, []).reduce((salad, ingredient) => {salad.addIngredient({name:ingredient}); return salad}, salad);
		salad = formKeys.filter(type => tempForm[type] instanceof Array).reduce((list, type) => ([...list, ...tempForm[type]]), []).filter(item => item.value).reduce((salad, ingredient) => {salad.addIngredient({name:ingredient.name}); return salad;}, salad);
		return salad;
	}

	validateForm() {
		const tempForm = this.state.formData;
		const formKeys = Object.keys(tempForm);
		const singleValid = formKeys.filter(type => typeof tempForm[type] === "string").map(type => tempForm[type]).reduce((result, value) => {return (result && !!value)}, true);
		const multipleValid = formKeys.filter(type => tempForm[type] instanceof Array).map(type => this.state.formErrors[type].validity).reduce((result, value) => {return (result && value)}, true);
		return singleValid && multipleValid;
	}

	getType(name) {
		return Object.keys(this.state.formData).filter(type => this.inventory[name][type] !== undefined)[0];
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit} className="text-primary" noValidate>
				<div className="form-group">
					<label>Välj bas</label>
					<SaladSelect name="foundation" selected={this.state.formData.foundation} invalidMessage="Var snäll och välj bas." onChange={this.handleChange} data={this.getSelectData('foundation')} />
				</div>
				<div className="form-group">
					<label>Välj proteiner</label>
					<SaladCheckbox validity={this.checkGroupValidity('protein')} invalidMessage="Var snäll och välj 1 eller 2 proteiner." onChange={this.handleCheckbox} data={this.getCheckboxData('protein')} />
				</div>
				<div className="form-group">
					<label>Välj extras</label>
					<SaladCheckbox validity={this.checkGroupValidity('extra')} invalidMessage="Var snäll och välj minst 4 men inte mer än 15 extras." onChange={this.handleCheckbox} data={this.getCheckboxData('extra')} />
				</div>
				<div className="form-group">
					<label>Välj dressing</label>
					<SaladSelect name="dressing" selected={this.state.formData.dressing} invalidMessage="Var snäll och välj dressing." onChange={this.handleChange} data={this.getSelectData('dressing')} />
				</div>
				<button type="submit" className="btn btn-primary" value="Submit">Submit</button>
			</form>
		);
	}
}

export default ComposeSalad;
