import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class SaladSelect extends Component {
	constructor(props) {
		super(props);
		this.handleChange = props.onChange.bind(this);
	}

	render() {
		return (
		<>
			<div className="input-group">
				<select name={this.props.name} onChange={this.handleChange} defaultValue={this.props.selected} className="custom-select" required>
				<option defaultValue value=""></option>
				{this.props.data.map(item =>
					<option value={item.value} key={item.value}>{item.text}</option>)
				}
			</select>
				<div className="input-group-append">
					<Link to={"/view-ingredient/" + this.props.selected}>
						<button className="btn btn-primary" type="button">?</button>
					</Link>
				</div>
			</div>
			<div className="invalid-feedback">{this.props.invalidMessage}</div>
		</>
		);
	}
}

SaladSelect.propTypes = {
  name: PropTypes.string,
  data: PropTypes.array,
  onChange: PropTypes.func,
  selected: PropTypes.string,
  prompt : PropTypes.string
};

export default SaladSelect;
