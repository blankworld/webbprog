import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SaladCheckbox extends Component {
	constructor(props) {
		super(props);
		this.handleCheckbox = props.onChange.bind(this);
	}
	
	render() {
		/*const scrollableDiv = {
			height: '150px',
			overflow: 'auto',
			paddingLeft:'24px'
		};*/
	
		return (
		<>
			<div style={(!this.props.validity) ? {display:"block"} : {display:"none"}} className="alert alert-danger" role="alert">{this.props.invalidMessage}</div>
			{this.props.data.map(item =>
			<div className="custom-control custom-checkbox" key={item.name + 2}>
				<input noValidate type="checkbox" className="custom-control-input" onChange={this.handleCheckbox} key={item.name} value={item.value} id={item.name} name={item.name} checked={item.value}/>
				<label key={item.name + 1} className="custom-control-label" htmlFor={item.name}>{item.text}</label>
				<Link style={{float:'right'}} key={item.name + 3} to={"/view-ingredient/" + item.name}>
					<span key={item.name + 4} className="badge badge-primary badge-pill">?</span>
				</Link>
			</div>)
			}
		</>
		);
	}
}

export default SaladCheckbox;