import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroesComponent }      from './heroes/heroes.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const routes: Routes = [
  { path: ':id', component: HeroesComponent },
  { path: '', component: HeroesComponent },
  { path: 'dashboard', component: DashboardComponent },
  //{ path: '', redirectTo: '/search', pathMatch: 'full' }, //add deafult route, redirect to dashboard
  { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], // configures the router at the root level
  exports: [RouterModule]
})
export class AppRoutingModule { }
