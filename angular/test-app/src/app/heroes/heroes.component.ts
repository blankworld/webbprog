import { Component, OnInit } from '@angular/core';
import Hero from './hero';
//import {HEROES} from './mock-heroes'; //HEROES is a imported constant array, why blackets.
import { HeroService } from '../hero.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-heroes', //CSS element selector
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  //heroes = HEROES; //property with hero vector
  heroes: Hero[];

  constructor(private heroService: HeroService,
  private location: Location,
  private route: ActivatedRoute) {
    //When Angular creates a HeroesComponent, the Dependency Injection system sets the heroService parameter to the singleton instance of HeroService.
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes); //subscribe is called when Observable updates
  }

  ngOnInit() { //called after component is created
    this.getHeroes();
  }

}
