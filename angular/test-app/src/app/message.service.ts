import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = [];
  movies: any[] = [];
  books: any[] = [];
  connectionData: any[];

  add(message: string) {
    this.messages.push(message);
  }

  getMovies() {
    return this.movies;
  }

  private addMessage(data: any) {
    this.messages.push(JSON.stringify(data));
  }

  setMovies(data: Promise<any>) {
    this.setResult("movies", data);
  }

  setBooks(data: Promise<any>) {
    this.setResult("books", data);
  }

  setConnectionData(data: Promise<any>) {
    this.setResult("connectionData", data);
  }

  private setResult(type:string, data:any) {
    data.then(result => {
      if (result !== null) {
        this[type] = result;
        this.addMessage(result);
        console.log(result)
      }
      });
  }

  clear() {
    this.messages = [];
  }
}
