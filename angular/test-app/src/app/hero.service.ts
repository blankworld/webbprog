import { Injectable } from '@angular/core';
import Hero from './heroes/hero';
import { HEROES } from './heroes/mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root' // root injector to include provider for HeroService
  // When you provide the service at the root level, Angular creates a single, shared instance of HeroService and injects into any class that asks for it.
})
export class HeroService {

  constructor(private http: HttpClient, private messageService: MessageService) {}

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    this.search("avatar");
    //this.fecthBookConnections("0312870019");
    this.fecthMovieConnections("tt0120783").then(connections =>
      this.messageService.setConnectionData(Promise.all(connections.map(connection => this.fetchBook(connection.isbn)))));
      //this.fetchMovie("tt0120783").then(result => console.log(result))
    return of(HEROES); //returns a Observable of type hero
  }

  search(param: string) {
    this.messageService.setMovies(this.fetchMovies(param));
    this.messageService.setBooks(this.fetchBooks(param));
  }

  getHero(id: number): Observable<Hero> {
  // TODO: send the message _after_ fetching the hero
    return of(HEROES.find(hero => hero.id === id)); // find returns the first acceptable element
  }

  private fetch(url: string): any {
    return this.http.get(url).toPromise().catch(response => response.json());
  }

  fetchMovies(key: string): any {
    const url = "http://www.omdbapi.com/?apikey=f22abc29&s=" + this.keyEncoder(key);
    return this.fetch(url)
    .then(object => object.Search.map(data => this.filterData(["Type", "imdbID", "Poster", "Title", "Year"], data))
    .filter(media => (media.Type !== "game"))
    .map(entry => ({...entry, "url":"https://www.imdb.com/title/" + entry.imdbID})))
    .then(entries => this.getValidEntries(entries)).catch(error => null);
  }

  fetchBooks(key: string): any {
    const url = "http://openlibrary.org/search.json?title=" + this.keyEncoder(key);
    return this.fetch(url)
    .then(object => object.docs.map(data => this.filterData(["title", "author_name", "isbn"], data)))
    .then(entries => this.getValidEntries(entries))
    .then(entries => this.uniqueEntries(entries, "isbn")
    .map(result => ({...result, "isbn":result.isbn[0], "cover":"http://covers.openlibrary.org/b/isbn/" + result.isbn[0] + "-M.jpg"})))
    .catch(error => null);
  }

  fecthMovieConnections(key:string) {
    return this.fetchConnections(key, "imdbID", "isbn");
  }

  fecthBookConnections(key:string) {
    return this.fetchConnections(key, "isbn", "imdbID");
  }

  fetchMovie(imdbID:string): any {
    const url = "http://www.omdbapi.com/?apikey=f22abc29&i=" + imdbID;
    return this.fetch(url)
    .then(object => ({...this.filterData(["Type", "imdbID", "Poster", "Title", "Year"], object), "url":"https://www.imdb.com/title/" + imdbID}))
    .catch(error => null);
  }

  fetchBook(isbn:string): any {
    const url = "http://openlibrary.org/api/books?format=json&jscmd=data&bibkeys=ISBN:" + isbn;
    return this.fetch(url)
    .then(object => this.filterData(["title", "authors"], object["ISBN:" + isbn]))
    .then(result => ({"title":result.title, "isbn":isbn, "author_name":result.authors.reduce((authors, author) => ([...authors, author.name]), []), "cover":"http://covers.openlibrary.org/b/isbn/" + isbn + "-M.jpg"}))
    .catch(error => null);
  }

  private fetchConnections(key: string, sourceType:string, targetType: string): any {
    const url = "http://localhost:3000/" + sourceType +"?" + sourceType + "=" + key;
    return this.fetch(url).catch(error => null);
  }

  private filterData(keys: any[], data: any): any {
    return keys.map(key => ({[key]: data[key]}))
    .reduce((result, data) => ({...result, ...data}), {});
  }

  private uniqueEntries(data: any[], noCompare: string): any[] {
    let temp = [];
    if (data.length > 1) {
      temp = [...this.uniqueEntries(data.filter(next => !this.compareObjects(next, data[0], noCompare)), noCompare), data[0]];
    } else {
      temp = [...data];
    }
    return temp;
  }

  private getValidEntries(data: any): any {
    const keys = Object.keys(data[0]);
    return data.filter(entry => keys.map(key => (entry[key] !== undefined)).reduce((result, check) => {result = result && check; return result;}, true));
  }

  private compareObjects(obj1: any, obj2: any, noCompare: string): boolean {
    return this.objectString(obj1, noCompare) === this.objectString(obj2, noCompare);
  }

  private objectString(obj: any, ignoreProp: string): string {
    return Object.keys(obj).reduce((text, key) => {
      if (key !== ignoreProp) {
        text += obj[key].toString().toLowerCase().trim();
      }
      return text;
    }, "");
  }

  private keyEncoder(value: string): string {
    const tempValue = value.toLowerCase().split(" ").reduce((newKey, keyPart) => (newKey += keyPart + "+"), "");
    return tempValue.slice(0, tempValue.length - 1)
  }

}
