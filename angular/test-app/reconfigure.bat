@echo off
if exist package-lock.json del package-lock.json
if exist node_modules rmdir /S node_modules
if exist dist rmdir /S dist
npm install
